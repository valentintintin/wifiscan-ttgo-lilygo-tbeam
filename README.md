# TTGO T-Beam Wi-Fi Scanner

## Output format

This uses a CSV format; the column and row separator can be defined by editing
`COLUMN_SEPARATOR` and `ROW_SEPARATOR`.

* **lat**: Latitude.
  Will be empty when the GPS coordinates are unknown or not modified since the last line of output.

* **lng**: Longitude.
  Will be empty when the GPS coordinates are unknown or not modified since the last line of output.

* **hdop**: Horizontal dilution of precision, in meters.
  Will be empty when the GPS coordinates are unknown or not modified since the last line of output.

* **age**: Time, in milliseconds, since the GPS location was updated.
  Will be empty when the GPS coordinates are unknown or not modified since the last line of output.

* **ssid**: ESSID of the network. Up to 32 characters.

* **channel**: Channel number of the network.

* **signal**: Received power, in dBm.

* **encryption**: Encryption of the network: `NONE`, `WEP`, `WPA1`, `WPA2`, `WPA1+2`, `802.1X`.

* **bssid**: BSSID of the network, as 12 hexadecimal digits separated by colons.
